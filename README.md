# Web and Cloud Project - Meal Explorer
This web application, "Meal Explorer", is a platform designed to help food enthusiasts discover new recipes from around the globe with just a few clicks. It utilizes TheMealDB API.
Our solution connects you with an extensive database of recipes. Whether you're craving something specific or looking for inspiration, our platform caters to every palate.
On our homepage, you're greeted with a user-friendly interface offering four key filters: meal name, ingredient, category, and country. 

## Authors
Federica Palermo - 60117@novasbe.pt
Giulia Pepe - 61909@novasbe.pt
Martim Costa - 39358@novasbe.pt

