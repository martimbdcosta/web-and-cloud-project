document.addEventListener('DOMContentLoaded', () => {
    fetchCategories();
    fetchAreas();
    fetchIngredients();
    autocompleteMealName(document.getElementById('meal-name-input'));
    initializeMap();
});

window.addEventListener('load', () => {
    initializeMap();
});

document.getElementById('find-meal-btn').addEventListener('click', fetchMeals);
document.getElementById('reset-filters-btn').addEventListener('click', resetFilters);

function fetchCategories() {
    fetch('https://www.themealdb.com/api/json/v1/1/list.php?c=list')
        .then(response => response.json())
        .then(data => populateDropdown(data.meals, 'category-select'))
        .catch(error => console.error('Error fetching categories:', error));
}

function fetchAreas() {
    fetch('https://www.themealdb.com/api/json/v1/1/list.php?a=list')
        .then(response => response.json())
        .then(data => populateDropdown(data.meals, 'area-select'))
        .catch(error => console.error('Error fetching areas:', error));
}

function populateDropdown(items, dropdownId) {
    const select = document.getElementById(dropdownId);

    items.forEach(item => {
        const value = item.strCategory || item.strArea;

        // Skip adding "Miscellaneous" to the category dropdown
        if (dropdownId === 'category-select' && value === "Miscellaneous") return;

        // Skip adding "Unknown" to the area dropdown
        if (dropdownId === 'area-select' && value === "Unknown") return;

        // Add item to the dropdown
        const option = document.createElement('option');
        option.value = value;
        option.textContent = value;
        select.appendChild(option);
    });
}

function fetchMeals() {
    const ingredient = document.getElementById('ingredient-input').value.trim();
    const category = document.getElementById('category-select').value;
    const area = document.getElementById('area-select').value;
    const mealName = document.getElementById('meal-name-input').value.trim();

    let urls = [];

    // Add URL for meal name search if provided
    if (mealName) {
        urls.push(`https://www.themealdb.com/api/json/v1/1/search.php?s=${mealName}`);
    }
    
    if (ingredient) {
        urls.push(`https://www.themealdb.com/api/json/v1/1/filter.php?i=${ingredient}`);
    }
    if (category && category !== "") {
        urls.push(`https://www.themealdb.com/api/json/v1/1/filter.php?c=${category}`);
    }
    if (area && area !== "") {
        urls.push(`https://www.themealdb.com/api/json/v1/1/filter.php?a=${area}`);
    }

    // Check if no filter is applied
    if (urls.length === 0) {
        alert("Please enter a meal name, ingredient, select a category, or select a country's cuisine for your search.");
        return;
    }

    Promise.all(urls.map(url => fetch(url).then(response => response.json())))
        .then(dataArrays => {
            let combinedMeals = [];
            if (dataArrays.length > 0) {
                combinedMeals = dataArrays[0].meals;

                for (let i = 1; i < dataArrays.length; i++) {
                    let currentMeals = dataArrays[i].meals;
                    combinedMeals = combinedMeals.filter(meal => currentMeals.some(m => m.idMeal === meal.idMeal));
                }
            }
            
            if (combinedMeals.length === 0) {
                // Display a message when no meals match the combined filters
                displayNoMealsFoundMessage();
            } else {
                displayMeals(combinedMeals);
            }
        })
        .catch(err => console.error("Error fetching meal data:", err));
}


function displayNoMealsFoundMessage() {
    const mealsDisplay = document.getElementById('meals-display');
    mealsDisplay.innerHTML = `<p>There aren't any meals matching your criteria. Try adjusting your search filters!</p>`;
}




function displayMeals(meals) {
    const mealsDisplay = document.getElementById('meals-display');
    mealsDisplay.innerHTML = '';

    if (meals) {
        meals.forEach(meal => {
            const mealElement = document.createElement('div');
            mealElement.classList.add('meal-item');
            mealElement.innerHTML = `
                <div class="meal-img">
                    <img src="${meal.strMealThumb}" alt="Meal Image">
                </div>
                <div class="meal-name">
                    <h3>${meal.strMeal}</h3>
                    <button class="recipe-btn" data-id="${meal.idMeal}">Get Recipe</button>
                </div>
            `;
            mealsDisplay.appendChild(mealElement);
        });
        attachEventToMealButtons();
    } else {
        mealsDisplay.innerHTML = `<p>Sorry, we couldn't find any meals. Try a different search!</p>`;
    }
}

function attachEventToMealButtons() {
    const recipeButtons = document.querySelectorAll('.recipe-btn');
    recipeButtons.forEach(button => {
        button.addEventListener('click', function() {
            const mealID = this.getAttribute('data-id');
            fetchMealDetails(mealID);
        });
    });
}

function fetchMealDetails(mealID) {
    fetch(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${mealID}`)
        .then(response => response.json())
        .then(data => displayMealModal(data.meals[0]))
        .catch(err => console.error("Error fetching meal details:", err));
}

function displayMealModal(meal) {
    const mealModal = document.getElementById('meal-modal');
    const mealDetails = document.getElementById('meal-details');
    
    mealDetails.innerHTML = `
        <h2 class="recipe-title">${meal.strMeal}</h2>
        <p class="recipe-category">${meal.strCategory}</p>
        <div class="recipe-instruct">
            <h3>Instructions:</h3>
            <p>${meal.strInstructions}</p>
        </div>
        <div class="recipe-meal-img">
            <img src="${meal.strMealThumb}" alt="Meal Image">
        </div>
    `;
    
    mealModal.style.display = 'block';

    document.getElementById('close-modal').addEventListener('click', () => {
        mealModal.style.display = 'none';
    });

    window.onclick = function(event) {
        if (event.target == mealModal) {
            mealModal.style.display = "none";
        }
    };
}

// Function to fetch ingredients for autocomplete from TheMealDB API
function fetchIngredients() {
    fetch('https://www.themealdb.com/api/json/v1/1/list.php?i=list')
        .then(response => response.json())
        .then(data => initializeAutocomplete(data.meals.map(meal => meal.strIngredient)))
        .catch(error => console.error('Error fetching ingredients:', error));
}
// Function to debounce actions
function debounce(func, wait) {
    let timeout;

    return function executedFunction(...args) {
        const later = () => {
            clearTimeout(timeout);
            func.apply(this, args);
        };

        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
    };
};

// Function to close all autocomplete lists in the document
function closeAllAutocompleteLists(elmnt) {
    var items = document.getElementsByClassName("autocomplete-items");
    for (let i = 0; i < items.length; i++) {
        if (elmnt != items[i] && elmnt != items[i].getElementsByTagName('input')[0]) {
            items[i].parentNode.removeChild(items[i]);
        }
    }
}

// Event listener to close all autocomplete lists when someone clicks in the document
document.addEventListener("click", function (e) {
    closeAllAutocompleteLists(e.target);
});

// Function for meal name autocomplete
function autocompleteMealName(inputElement) {
    inputElement.addEventListener("input", debounce(function(e) {
        let val = this.value;
        closeAllAutocompleteLists();
        if (!val) return false;

        const autocompleteList = document.createElement("DIV");
        autocompleteList.setAttribute("id", this.id + "autocomplete-list");
        autocompleteList.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(autocompleteList);

        fetch(`https://www.themealdb.com/api/json/v1/1/search.php?s=${val}`)
            .then(response => response.json())
            .then(data => {
                // If no meals were found, show a message
                if (!data.meals) {
                    const item = document.createElement("DIV");
                    item.innerHTML = `<span class='no-match'>No matches found</span>`;
                    autocompleteList.appendChild(item);
                    return; // Exit early since no meals were found
                }

                // Otherwise, continue to list the matching meals
                data.meals.forEach(meal => {
                    if (meal.strMeal.toLowerCase().startsWith(val.toLowerCase())) {
                        const item = document.createElement("DIV");
                        item.innerHTML = `<strong>${meal.strMeal.substr(0, val.length)}</strong>${meal.strMeal.substr(val.length)}`;
                        item.innerHTML += `<input type='hidden' value='${meal.strMeal}'>`;
                        item.addEventListener("click", function() {
                            inputElement.value = this.getElementsByTagName("input")[0].value;
                            closeAllAutocompleteLists();
                        });
                        autocompleteList.appendChild(item);
                    }
                });
            })
            .catch(error => {
                console.error('Error fetching autocomplete suggestions:', error);
            });
    }, 500));
}

// Function to initialize ingredient autocomplete feature
function initializeAutocomplete(ingredientsList) {
    const input = document.getElementById('ingredient-input');
    input.addEventListener("input", debounce(function() {
        let value = this.value.trim();
        closeAllAutocompleteLists();
        if (!value) return false;

        const autocompleteList = document.createElement("DIV");
        autocompleteList.setAttribute("id", this.id + "autocomplete-list");
        autocompleteList.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(autocompleteList);

        // Filtering the ingredientsList based on the input value
        const matches = ingredientsList.filter(ingredient => ingredient.toLowerCase().startsWith(value.toLowerCase()));

        // If there are no matches, display a no match message
        if (matches.length === 0) {
            const item = document.createElement("DIV");
            item.innerHTML = `<span class='no-match'>No matches found</span>`;
            autocompleteList.appendChild(item);
        } else {
            // Otherwise, continue to list the matching ingredients
            matches.forEach(ingredient => {
                const item = document.createElement("DIV");
                item.innerHTML = `<strong>${ingredient.substr(0, value.length)}</strong>${ingredient.substr(value.length)}`;
                item.innerHTML += `<input type='hidden' value='${ingredient}'>`;
                item.addEventListener("click", function() {
                    input.value = this.getElementsByTagName("input")[0].value;
                    closeAllAutocompleteLists();
                });
                autocompleteList.appendChild(item);
            });
        }
    }, 500));
}

function resetFilters() {
    // Reset the value of the input fields
    document.getElementById('meal-name-input').value = '';
    document.getElementById('ingredient-input').value = '';

    // Reset the dropdowns to their default value
    document.getElementById('category-select').selectedIndex = 0;
    document.getElementById('area-select').selectedIndex = 0;

    // Clear any displayed meals
    const mealsDisplay = document.getElementById('meals-display');
    mealsDisplay.innerHTML = '';
}

function initializeMap() {
    var map = L.map('map').setView([20, 0], 2);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap contributors'
    }).addTo(map);

    fetch('https://www.themealdb.com/api/json/v1/1/list.php?a=list')
        .then(response => response.json())
        .then(data => {
            data.meals.forEach(area => {
                const coords = getAreaCoords(area.strArea);
                if (coords) {
                    const marker = L.marker([coords.lat, coords.lng]).addTo(map);
                    marker.bindPopup(`<b>Area:</b> ${area.strArea}`);
                    marker.on('click', () => fetchMealsByCountry(area.strArea));
                }
            });
        })
        .catch(error => console.error('Errore nel caricamento delle aree:', error));
}

function getAreaCoords(areaName) {
    const areaCoords = {
        "American": { lat: 37.0902, lng: -95.7129 },
        "British": { lat: 55.3781, lng: -3.4360 },
        "Canadian": { lat: 56.1304, lng: -106.3468 },
        "Chinese": { lat: 35.8617, lng: 104.1954 },
        "Croatian": { lat: 45.1, lng: 15.2 },
        "Dutch": { lat: 52.1326, lng: 5.2913 },
        "Egyptian": { lat: 26.8206, lng: 30.8025 },
        "Filipino": { lat: 12.8797, lng: 121.7740 },
        "French": { lat: 46.2276, lng: 2.2137 },
        "Greek": { lat: 39.0742, lng: 21.8243 },
        "Indian": { lat: 20.5937, lng: 78.9629 },
        "Irish": { lat: 53.4129, lng: -8.2439 },
        "Italian": { lat: 41.9028, lng: 12.4964 },
        "Jamaican": { lat: 18.1096, lng: -77.2975 },
        "Japanese": { lat: 36.2048, lng: 138.2529 },
        "Kenyan": { lat: -0.0236, lng: 37.9062 },
        "Malaysian": { lat: 4.2105, lng: 101.9758 },
        "Mexican": { lat: 23.6345, lng: -102.5528 },
        "Moroccan": { lat: 31.7917, lng: -7.0926 },
        "Polish": { lat: 51.9194, lng: 19.1451 },
        "Portuguese": { lat: 39.3999, lng: -8.2245 },
        "Russian": { lat: 61.5240, lng: 105.3188 },
        "Spanish": { lat: 40.4637, lng: -3.7492 },
        "Thai": { lat: 15.8700, lng: 100.9925 },
        "Tunisian": { lat: 33.8869, lng: 9.5375 },
        "Turkish": { lat: 38.9637, lng: 35.2433 },
        "Vietnamese": { lat: 14.0583, lng: 108.2772 },
    };

    return areaCoords[areaName] || null;
}

function fetchMealsByCountry(area) {
    fetch(`https://www.themealdb.com/api/json/v1/1/filter.php?a=${area}`)
        .then(response => response.json())
        .then(data => {
            displayMeals(data.meals);
        })
        .catch(error => console.error('Errore nel recupero dei pasti per l\'area:', error));
}

